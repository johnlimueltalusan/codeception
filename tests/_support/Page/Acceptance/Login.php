<?php
namespace Page\Acceptance;

class Login
{
    /*This is where you put your page objects a.k.a storage of xpaths*/
    public static $URL = 'https://bank.paysera.com/en/login';
    public static $TITLE = "//div[@class='panel-title']";
    public static $IDENTIFIER_TXTBOX = "//input[@id='userIdentifier']";
    public static $LOGIN_BTN = "//button[@type='submit']";
    public static $PASSWORD_DRPDWN = "//strong[contains(text(),'Password')]";
    public static $PASSWORD_TXTBOX = "//input[@id='password']";
    public static $ACCOUNT_OVERVIEW = "//h1[@class='page-title']";
    public static $IDENTIFIER_ERR_MSG = "//div[contains(text(),'The specified user could not be found')]";
    public static $PASSWORD_ERR_MSG = "//span[contains(text(),'Incorrect password. Please try again.')]";
    public static $LOGOUT_BTN = "(//span[contains(text(),'Log out')])[1]";



    public static function route($param)
    {
        return static::$URL.$param;
    }

    /**
     * @var \AcceptanceTester;
     */
    protected $acceptanceTester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->acceptanceTester = $I;
    }

}
