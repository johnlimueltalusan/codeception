<?php
use Page\Acceptance\Login;
    
/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
*/

class AcceptanceTester extends \Codeception\Actor
{
	use _generated\AcceptanceTesterActions;

    /**
     * @Given I am on page
     */
     public function iAmOnPage()
     {
        $this->amOnPage(Login::$URL);
     }

    /**
     * @Given I see the Login title, Email or Phone Number textbox and Login button
     */
     public function iSeeTheLoginTitleEmailOrPhoneNumberTextboxAndLoginButton()
     {
		$this->waitForElement(Login::$TITLE, 5); 
		$this->waitForElement(Login::$IDENTIFIER_TXTBOX, 5);  
     }

   /**
     * @When I input my correct email :arg1
     */
     public function iInputMyCorrectEmail($arg1)
     {
     	$this->wait(0.3);
        $this->fillField(Login::$IDENTIFIER_TXTBOX, $arg1);
     }

    /**
     * @When I click the username page's LOG IN button
     */
     public function iClickTheUsernamePagesLOGINButton()
     {
     	$this->click(Login::$LOGIN_BTN);
     }

    /**
     * @When I click the password dropdown button
     */
     public function iClickThePasswordDropdownButton()
     {
     	$this->waitForElement(Login::$PASSWORD_DRPDWN, 10); 
     	$this->click(Login::$PASSWORD_DRPDWN);
     }

    /**
     * @When I input my correct password :arg1
     */
     public function iInputMyCorrectPassword($arg1)
     {
     	$this->wait(0.3);
    	$this->FillField(Login::$PASSWORD_TXTBOX, $arg1);
     }

    /**
     * @When I click the password page's LOG IN button
     */
     public function iClickThePasswordPagesLOGINButton()
     {
		$this->click(Login::$LOGIN_BTN);
	 }

    /**
     * @Then I should see my dashboard
     */
     public function iShouldSeeMyDashboard()
     {
        $this->waitForElement(Login::$ACCOUNT_OVERVIEW, 15); 
     }

         /**
     * @When I input my incorrect email :arg1
     */
     public function iInputMyIncorrectEmail($arg1)
     {
     	$this->wait(0.3);
        $this->fillField(Login::$IDENTIFIER_TXTBOX, $arg1);
     }

    /**
     * @Then I should see the incorrect email error message
     */
     public function iShouldSeeTheIncorrectEmailErrorMessage()
     {
        $this->waitForElement(Login::$IDENTIFIER_ERR_MSG, 10);
     }
         /**
     * @Then I should see the incorrect password error message
     */
     public function iShouldSeeTheIncorrectPasswordErrorMessage()
     {
        $this->waitForElement(Login::$PASSWORD_ERR_MSG, 10);
     }

    /**
     * @Then I should not see any error message
     */
     public function iShouldNotSeeAnyErrorMessage()
     {
        $this->dontSeeElement(Login::$IDENTIFIER_ERR_MSG);
     }

    /**
     * @Then I logout of the website
     */
     public function iLogoutOfTheWebsite()
     {
     	$this->waitForElement(Login::$LOGOUT_BTN, 10);
        $this->click(Login::$LOGOUT_BTN);
     }
}