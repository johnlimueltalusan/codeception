Feature: Login
#To run all of the scenarios : php vendor/bin/codecept run path/to/login.feature --steps
#Example : php vendor/bin/codecept run tests/login.feature --steps

#To run a specific scenario : php vendor/bin/codecept run -g <insert scenario name> path/to/login.feature --steps
#Example : php vendor/bin/codecept run -g happy_path tests/login.feature --steps

#You can use alias, alias crun="php vendor/bin/codecept run"
#So you can run it as "crun tests/login.feature --steps"

#Note: I didn't create a scenario for login via mobile number as it's only available for Lithuanian customers, I think.

@happy_path
Scenario: Login with email and correct password
	Given I am on page
	  And I see the Login title, Email or Phone Number textbox and Login button
	 When I input my correct email "johnlimueltalusan@gmail.com"
	  And I click the username page's LOG IN button
	  And I click the password dropdown button
	  And I input my correct password "P@ssw0rd123."
	  And I click the password page's LOG IN button
	 Then I should see my dashboard
	  And I logout of the website

@incorrect_email
Scenario: Login with incorrect email
	Given I am on page
	  And I see the Login title, Email or Phone Number textbox and Login button
	 When I input my incorrect email "limuel@gmail.com"
	  And I click the username page's LOG IN button
	 Then I should see the incorrect email error message

@username_invalid_characters
Scenario: Login with incorrect email
	Given I am on page
	  And I see the Login title, Email or Phone Number textbox and Login button
	 When I input my incorrect email "!@#%^&*()(??/"
	  And I click the username page's LOG IN button
	 Then I should see the incorrect email error message

@correct_email_incorrect_password
Scenario: Login with incorrect email and incorrect password
	Given I am on page
	  And I see the Login title, Email or Phone Number textbox and Login button
	 When I input my correct email "johnlimueltalusan@gmail.com"
	  And I click the username page's LOG IN button
	  And I click the password dropdown button
	  And I input my correct password "P@ssw0rd123"
	  And I click the password page's LOG IN button
	 Then I should see the incorrect password error message

@correct_email_invalid_characters_password
Scenario: Login with incorrect email and incorrect password
	Given I am on page
	  And I see the Login title, Email or Phone Number textbox and Login button
	 When I input my correct email "johnlimueltalusan@gmail.com"
	  And I click the username page's LOG IN button
	  And I click the password dropdown button
	  And I input my correct password "!@#%^&*()(??/"
	  And I click the password page's LOG IN button
	 Then I should see the incorrect password error message

@blank_email
Scenario: Login with blank email
	Given I am on page
	  And I see the Login title, Email or Phone Number textbox and Login button
	 When I input my correct email ""
	  And I click the username page's LOG IN button
	 Then I should not see any error message